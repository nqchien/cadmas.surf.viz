from scipy.io import FortranFile
import numpy as np


f = FortranFile('data.rsl','r')

ivr001, ivr002 = f.read_ints(dtype=np.int32)
numi, numk, numb = f.read_ints(dtype=np.int32)
ln, lv, lp, lf, lk, ld, ls = f.read_ints(dtype=np.int32)
xx = f.read_ints(dtype=np.float)
zz = f.read_ints(dtype=np.float)
xx2 = f.read_ints(dtype=np.float) # grid spacing
zz2 = f.read_ints(dtype=np.float) # grid spacing
indx = f.read_ints(dtype=np.int32)
indz = f.read_ints(dtype=np.int32) 
ggv = f.read_reals(dtype=np.float)
ggx = f.read_reals(dtype=np.float)
ggz = f.read_reals(dtype=np.float)

print "Computational Domain: %ix%i" % (numk, numi)

# For uniform grid only
dx = 0.1
dz = 0.05
zgrid, xgrid = np.mgrid[slice(0,5.05,dz), slice(0,200.1,dx)]

measuredCellX = "198	400	589	960	1089	1189	1238	1280	1319	1387	1460	1541".split()
measuredCellX = [int(i) for i in measuredCellX]

tseries = []
zseries = []
fzs = open('zs.txt', 'w')

# time iteration 
for iter in range(999):
    print "Iteration:", iter
    rec0 = f.read_record([('nnow', 'int32'), ('tnow', 'float')])
    rec1 = f.read_record([('icgitr', 'int32'), ('dtnow', 'float'), ('cgbnrm', 'float'), 
        ('cgxnrm','float'), ('fsum','float'), ('fcut','float')])
    fsumin, fsumt, uumt, fsumaj = f.read_reals(dtype=np.float)  # ok till here, tested 
    nf = f.read_ints(dtype=np.int32)            # ok return -1 (ghost cells
    uu = f.read_reals(dtype=np.float)
    bcu = f.read_reals(dtype=np.float)
    ww = f.read_reals(dtype=np.float)
    bcw = f.read_reals(dtype=np.float)
    pp = f.read_reals(dtype=np.float)
    bcp = f.read_reals(dtype=np.float)
    ff = f.read_reals(dtype=np.float)
    bcf = f.read_reals(dtype=np.float)
    ak = f.read_reals(dtype=np.float)
    bck = f.read_reals(dtype=np.float)
    ae = f.read_reals(dtype=np.float)    # only if turbulence k-e 
    bce = f.read_reals(dtype=np.float)    # only if turbulence k-e
    # dd = f.read_reals(dtype=np.float)   # not used because NUMD=0, no scalar
    # bcd = f.read_reals(dtype=np.float)   # not used because NUMD=0, no scalar
    tbub = f.read_reals(dtype=np.float)    # only if turbulence k-e  
    tdrop = f.read_reals(dtype=np.float)    # only if turbulence k-e
    
    #print type(rec0)
    tnow = rec0[0][1]
    nfarray = nf.reshape(numk,numi)[1:-1, :-1]
    farray = ff.reshape(numk,numi)[1:-1, :-1]
    
    tseries.append(tnow)
    zseries.append([0] * len(measuredCellX))
    
    fzs.write(str(tnow) + '\t')
    
    count = 0
    for i in measuredCellX:
        try: 
            locws = np.where((nfarray[:,i]==5) | (nfarray[:,i]==2) | (nfarray[:,i]==1))[0][0]
            zs = locws * dz + farray[locws, i] * dz
            zseries[iter][count] = zs
            count += 1
            fzs.write(str(zs) + '\t')
        except:
            fzs.write('nan\t')
        
    fzs.write('\n')
    fzs.flush()
    
    
    # plt.pcolor(nf.reshape(numk,numi)[1:-1, :-1], cmap='Blues')
    # plt.imshow(nf.reshape(numk,numi)[1:-1, :-1], cmap='Blues', interpolation='bilinear')
    # plt.imshow(nf.reshape(numk,numi)[1:-1, :-1], cmap='Blues', interpolation='bicubic')
    # plt.pcolor(xgrid, zgrid, ak.reshape(numk,numi)[1:-1, :], cmap='Blues')
    # plt.plot([0,12], [0.375,0.375], 'r--')
    # plt.hold(True)
    # plt.quiver(xgrid[:,::10], zgrid[:,::10], uu.reshape(numk,numi)[1:-1, :], ww.reshape(numk,numi)[1:-1, :-1], scale=0.05)
    # plt.colorbar()
    #plt.show()  # disable if plot is written to file 
    # plt.savefig('ak_t%03i.png' % iter)
    # plt.close()
    
    
f.close()
fzs.close()
