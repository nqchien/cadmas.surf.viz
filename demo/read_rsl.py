from scipy.io import FortranFile
import numpy as np
import matplotlib.pyplot as plt


#~ f = FortranFile('data.rsl','r')
f = FortranFile('data.rsl','r')

ivr001, ivr002 = f.read_ints(dtype=np.int32)
numi, numk, numb = f.read_ints(dtype=np.int32)
ln, lv, lp, lf, lk, ld, ls = f.read_ints(dtype=np.int32)
xx = f.read_ints(dtype=np.float)
zz = f.read_ints(dtype=np.float)
xx2 = f.read_ints(dtype=np.float) # grid spacing
zz2 = f.read_ints(dtype=np.float) # grid spacing
indx = f.read_ints(dtype=np.int32)
indz = f.read_ints(dtype=np.int32) 
ggv = f.read_reals(dtype=np.float)
ggx = f.read_reals(dtype=np.float)
ggz = f.read_reals(dtype=np.float)

print "Computational Domain: %ix%i" % (numk, numi)

# For uniform grid only
dx = 0.1
dz = 0.05
zgrid, xgrid = np.mgrid[slice(0,5,dz), slice(0,200.1,dx)]

# time iteration 
for iter in range(10):
    print "Iteration:", iter
    rec0 = f.read_record([('nnow', 'int32'), ('tnow', 'float')])
    rec1 = f.read_record([('icgitr', 'int32'), ('dtnow', 'float'), ('cgbnrm', 'float'), 
        ('cgxnrm','float'), ('fsum','float'), ('fcut','float')])
    fsumin, fsumt, uumt, fsumaj = f.read_reals(dtype=np.float)  # ok till here, tested 
    nf = f.read_ints(dtype=np.int32)            # ok return -1 (ghost cells
    uu = f.read_reals(dtype=np.float)
    bcu = f.read_reals(dtype=np.float)
    ww = f.read_reals(dtype=np.float)
    bcw = f.read_reals(dtype=np.float)
    pp = f.read_reals(dtype=np.float)
    bcp = f.read_reals(dtype=np.float)
    ff = f.read_reals(dtype=np.float)
    bcf = f.read_reals(dtype=np.float)
    ak = f.read_reals(dtype=np.float)
    bck = f.read_reals(dtype=np.float)
    ae = f.read_reals(dtype=np.float)    # only if turbulence k-e 
    bce = f.read_reals(dtype=np.float)    # only if turbulence k-e
    # dd = f.read_reals(dtype=np.float)   # not used because NUMD=0, no scalar
    # bcd = f.read_reals(dtype=np.float)   # not used because NUMD=0, no scalar
    tbub = f.read_reals(dtype=np.float)    # only if turbulence k-e  
    tdrop = f.read_reals(dtype=np.float)    # only if turbulence k-e
    
    #print type(rec0)
    tnow = rec0[0][1]
    # plt.title('t = %.2f'% tnow)
    
    # plt.pcolor(ff.reshape(numk,numi)[1:-1, :-1], cmap='Blues')
    #plt.pcolor(xgrid, zgrid, ae.reshape(numk,numi)[1:-1, :-1], cmap='Blues')
    # plt.plot([0,2], [0.375,0.375], 'r--')
    #plt.quiver(xgrid[:,::10], zgrid[:,::10], uu.reshape(numk,numi)[1:-1, ::10], ww.reshape(numk,numi)[1:-1, :-1:10], scale=20)
    # plt.colorbar()
    #plt.show()  # disable if plot is written to file 
    # plt.savefig('nf_t%03i.png' % iter)
    plt.close()

def draw_details():
    uvel = uu.reshape(numk,numi)[20:47, 672:720]
    wvel = ww.reshape(numk,numi)[20:47, 672:720]
    uvel[5, 35] = 1.0
    wvel[5, 35] = 0.0
    
    plt.figure(figsize=(6,4))
    plt.pcolormesh(xgrid[20:47, 672:720], zgrid[20:47, 672:720], nf.reshape(numk,numi)[20:47, 1172:1220], cmap='Blues', )
    plt.quiver(xgrid[20:47, 672:720], zgrid[20:47, 672:720], uvel, wvel, scale=10)
    plt.xlabel('Cross-shore distance (m)')
    plt.ylabel('Elevation (m)')
    plt.annotate('1 m/s', (30.2, 0.52) )
    plt.show()

f.close()
