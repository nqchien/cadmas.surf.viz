## Tài liệu mẫu 

* `data.in`  file text đầu vào
* `cadmas.exe` file chạy Windows
* `cadmas_g95.exe` file chạy Windows biên tập bởi GNU Fortran 
* `cadmas_at`  file chạy Linux 
* `data.rsl`   file kết quả (result), dạng nhị phân, sau khi chạy chương trình 
* `find_zs.py`  mã lệnh Python đọc kết quả và tìm đường mặt nước 
* `read_rsl.py`  mã lệnh Python đọc kết quả và hiển thị