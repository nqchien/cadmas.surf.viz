# README #

Tài liệu README này đề cập nến mục tiêu và nội dung dự án. 

### Về chương trình CADMAS SURF ###

* Phát triển bởi [Coastal Development Institute for Technology](http://www.cdit.or.jp/), Nhật Bản 
* Là một "máng sóng số" 2 chiều, để mô phỏng sự truyền sóng tương tác sóng với công trình bờ biển
* Viết bằng ngôn ngữ lập trinh Fortran, là phần mềm nguồn mở

### Mục tiêu của dự án này ###

* CADMAS SURF là phần mềm có tính năng mạnh, tốc độ tính toán nhanh
* Tuy nhiên hiện nay chương trình sử dụng giao diện dòng lệnh, gây khó khăn cho việc nhập các thông số hình học như địa hình đáy, kích thước công trình
* Do vậy, cần có những công cụ đồ họa giúp nhập liệu và hiển thị kết quả tính toán.
* Một giao diện đồ họa (GUI) đầy đủ là khó khăn, nhưng có thể sử dụng các thư viện vẽ biểu đồ (plotting) để trợ giúp
* Ngôn ngữ lập trình được lựa chọn là Python, trước hết là vì Python có bộ thư viện rất phong phú giúp vẽ đồ thị cũng như đọc số liệu từ file nhị phân (binary).

### Quy tắc đóng góp ###

* Viết code bằng Python 
* Sử dụng thư viện Matplotlib
* Đọc file nhị phân tạo bởi Fortran

### Liên hệ ###

* Nguyễn Quang Chiến, GV bộ môn Quản lý tổng hợp vùng ven bờ, ĐH Thủy lợi
* Dương Kim Cương, SV lớp 57B, ĐH Thủy lợi