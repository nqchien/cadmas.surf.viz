from scipy.io import FortranFile
import numpy as np
import matplotlib.pyplot as plt


f = FortranFile('data400.500.rsl','r')

ivr001, ivr002 = f.read_ints(dtype=np.int32)
numi, numk, numb = f.read_ints(dtype=np.int32)
ln, lv, lp, lf, lk, ld, ls = f.read_ints(dtype=np.int32)
xx = f.read_ints(dtype=np.float)
zz = f.read_ints(dtype=np.float)
xx2 = f.read_ints(dtype=np.float) # grid spacing
zz2 = f.read_ints(dtype=np.float) # grid spacing
indx = f.read_ints(dtype=np.int32)
indz = f.read_ints(dtype=np.int32) 
ggv = f.read_reals(dtype=np.float)
ggx = f.read_reals(dtype=np.float)
ggz = f.read_reals(dtype=np.float)

print "Computational Domain: %ix%i" % (numk, numi)

# For uniform grid only
dx = 0.1
dz = 0.05
zgrid, xgrid = np.mgrid[slice(0,5,dz), slice(0,200.1,dx)]

def fread_timestep():
    rec0 = f.read_record([('nnow', 'int32'), ('tnow', 'float')])
    rec1 = f.read_record([('icgitr', 'int32'), ('dtnow', 'float'), ('cgbnrm', 'float'), 
        ('cgxnrm','float'), ('fsum','float'), ('fcut','float')])
    fsumin, fsumt, uumt, fsumaj = f.read_reals(dtype=np.float)  # ok till here, tested 
    nf = f.read_ints(dtype=np.int32)            # ok return -1 (ghost cells
    uu = f.read_reals(dtype=np.float)
    bcu = f.read_reals(dtype=np.float)
    ww = f.read_reals(dtype=np.float)
    bcw = f.read_reals(dtype=np.float)
    pp = f.read_reals(dtype=np.float)
    bcp = f.read_reals(dtype=np.float)
    ff = f.read_reals(dtype=np.float)
    bcf = f.read_reals(dtype=np.float)
    ak = f.read_reals(dtype=np.float)
    bck = f.read_reals(dtype=np.float)
    ae = f.read_reals(dtype=np.float)    # only if turbulence k-e 
    bce = f.read_reals(dtype=np.float)    # only if turbulence k-e
    # dd = f.read_reals(dtype=np.float)   # not used because NUMD=0, no scalar
    # bcd = f.read_reals(dtype=np.float)   # not used because NUMD=0, no scalar
    tbub = f.read_reals(dtype=np.float)    # only if turbulence k-e  
    tdrop = f.read_reals(dtype=np.float)    # only if turbulence k-e
    return rec0, rec1, fsumin, fsumt, uumt, fsumaj, nf, uu, bcu, ww, bcw, pp, bcp, ff, bcf, ak, bck, ae, bce, tbub, tdrop

def draw():
    plt.figure(figsize=(10,2))
    plt.pcolor(xgrid, zgrid, nf.reshape(numk,numi)[1:-1, :-1], cmap='Blues')
    plt.quiver(xgrid[:,::10], zgrid[:,::10], uu.reshape(numk,numi)[1:-1, ::10], ww.reshape(numk,numi)[1:-1, :-1:10], scale=20)
    plt.show()

def draw_details():
    uvel = uu.reshape(numk,numi)[43:73, 1300:1450]
    wvel = ww.reshape(numk,numi)[43:73, 1300:1450]
    uvel[3, 31] = 1.0
    wvel[3, 31] = 0.0
    
    plt.figure(figsize=(8,8))
    plt.pcolormesh(xgrid[43:73, 1300:1450], zgrid[43:73, 1300:1450], nf.reshape(numk,numi)[43:73, 1300:1450], cmap='Blues', )
    # plt.quiver(xgrid[43:73, 1300:1450], zgrid[43:73, 1300:1450], uvel, wvel, scale=2)
    plt.quiver(xgrid[43:73, 1300:1450:10], zgrid[43:73, 1300:1450:10], uvel[:,::10], wvel[:,::10], scale=2)
    plt.xlabel('Cross-shore distance (m)')
    plt.ylabel('Elevation (m)')
    plt.annotate('1 m/s', (134, 2.8) )
    plt.show()

# time iteration 
for iter in range(0):
    print "Iteration:", iter
    rec0, rec1, fsumin, fsumt, uumt, fsumaj, nf, uu, bcu, ww, bcw, pp, bcp, ff, bcf, ak, bck, ae, bce, tbub, tdrop = fread_timestep()
    
    #print type(rec0)
    tnow = rec0[0][1]
    # plt.title('t = %.2f'% tnow)
    # plt.pcolor(xgrid, zgrid, nf.reshape(numk,numi)[1:-1, :-1], cmap='Blues')
    #plt.pcolor(xgrid, zgrid, ae.reshape(numk,numi)[1:-1, :-1], cmap='Blues')
    # plt.plot([0,2], [0.375,0.375], 'r--')
    # plt.hold(True)
    #plt.quiver(xgrid[:,::10], zgrid[:,::10], uu.reshape(numk,numi)[1:-1, ::10], ww.reshape(numk,numi)[1:-1, :-1:10], scale=20)
    # plt.colorbar()
    #plt.show()  # disable if plot is written to file 
    # plt.savefig('nf_t%03i.png' % iter)
    # plt.close()

for iter in range(20):
    print "Iteration:", iter
    rec0, rec1, fsumin, fsumt, uumt, fsumaj, nf, uu, bcu, ww, bcw, pp, bcp, ff, bcf, ak, bck, ae, bce, tbub, tdrop = fread_timestep()
    draw_details()
    
    #print type(rec0)
    tnow = rec0[0][1]


f.close()
